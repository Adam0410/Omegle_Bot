#API IMPORTS
from selenium import webdriver
from time import sleep
from time import time

#USER VARIABLES
topics = [
    "Default"
]

msgPause = 2.5
requestNum = 1
feedbackNum = 1
technicalNum = 1

#------------------------------------------WEB HANDLING FUNCTIONS--------------------------------------------



def startChatBot():
    global driver
    driver = webdriver.Chrome("C:\Users\Adasli199\Desktop\OmegleBot\chromedriver.exe")
    driver.get("http://www.omegle.com/")

    for topic in topics:
        driver.find_element_by_css_selector("input.newtopicinput").send_keys(topic+"\n")

    driver.find_element_by_id("textbtn").click()



def newChat(disconnected):
    disconnectBtn = driver.find_element_by_class_name("disconnectbtn")

    if disconnected == True:

        while True:

            try:
                disconnectBtn.click()
                break
            except:
                pass

    else:

        try:
            disconnectBtn.click()
            disconnectBtn.click()
            disconnectBtn.click()
        except:
            sleep(0.1)

            while True:

                try:
                    disconnectBtn.click()
                    break
                except:
                    pass


def waitForChat():
    global messageBox
    global messageSend
    startTime = time()

    while True:
        sleep(0.1)
        tempLog = getTempLog()
        currTime = time()

        if tempLog[0:6] == "You're":

            if tempLog[(len(tempLog)-10):len(tempLog)] != "interests!":

                    try:
                        messageBox = driver.find_element_by_css_selector("textarea.chatmsg")
                        messageSend = driver.find_element_by_css_selector("button.sendbtn")
                        updateLog()
                        break
                    except:
                        newChat(True)

            elif not isDisconnected():
                newChat(False)
            else:
                newChat(True)
        elif tempLog[0:5] == "Error":
            newChat(True)
        elif tempLog[0:7] == "Looking" and currTime - startTime > 20:
            newChat(False)
            startTime = time()




def getTempLog():

    while True:

        try:
            tempLog = driver.find_element_by_class_name("logwrapper").text
            break
        except:
            pass

    tempLog = tempLog.encode('utf-8')

    if tempLog[(len(tempLog)-21):] == "Stranger is typing...":
        tempLog = tempLog[:(len(tempLog)-22)]

    return tempLog



def updateLog():
    global log
    log = getTempLog()



def isDisconnected():
    tempLog = getTempLog()

    if tempLog[(len(tempLog)-10):] == "(Settings)":
        return True
    elif tempLog[(len(tempLog)-6):] == "reddit" and tempLog[(len(tempLog)-16):(len(tempLog)-9)] == "Twitter":
        return True
    else:
        return False



def checkForMessage():
    tempLog = getTempLog()

    if tempLog == log:
        return False, ""
    elif isDisconnected():
        return "Disconnected", ""
    else:
        msg = tempLog[(len(log)+11):]
        updateLog()
        return True, msg



def waitForMessage(maxWaitTime):
    timeWarn = False
    startTime = time()

    while True:
        status, msg = checkForMessage()

        if status == True:
            return True, msg
        elif status == "Disconnected":
            return "Disconnected", ""
        else:
            currTime = time()

            if currTime - startTime > maxWaitTime:
                return False, ""
            elif currTime - startTime > maxWaitTime - 60 and timeWarn == False:
                timeWarn = True
                state = sendMessage("You only have 1 minute left to send me a message before I'll have to disconnect.")

                if state == "Disconnected":
                    return "Disconnected", ""

        sleep(0.1)



def sendMessage(msg):

    try:
        messageBox.send_keys(msg)
    except:
        return "Disconnected"

    try:
        messageSend.click()
    except:
        return "Disconnected"

    updateLog()


#------------------------------------------ROBOT SPEECH AND FEEDBACK FUNCTIONS------------------------------------------------



def openingMessage():
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"



def mainUsrMenu():

    while True:
        state, msg = waitForMessage(120)
        msg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        else:

            if msg == "default":
                state = descriptionMessage()

                if state == "Disconnected":
                    return "Disconnected"
                elif state == False:
                    return False

            elif msg == "Default":
                technicalInfoMsg()
                pass
            elif msg == "Default":
                getFeedback()
                pass
            else:
                state = sendMessage("Default")

                if state == "Disconnected":
                    return "Disconnected"



def descriptionMessage():
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    while True:
        state, msg = waitForMessage(120)
        msg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        elif msg == "contact":
            state = getContactDetails()

            if state == "Disconnected":
                return "Disconnected"
            elif state == False:
                return False
            else:
                return True

        elif msg == "back":
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"

            return True
        else:
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"



def getContactDetails():
    global name
    global age
    global location
    global skypeUsr
    global partnerDesc

    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Default")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)

    while True:
        state, msg = waitForMessage(120)
        lowerMsg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        elif lowerMsg == "back":
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"

            return True
        else:
            name = msg
            state = sendMessage("Is your name "+name+"? Type 'Yes' or 'No'")

            if state == "Disconnected":
                return "Disconnected"

            correctName = False

            while True:
                state, msg = waitForMessage(120)
                msg = msg.lower()

                if state == "Disconnected":
                    return "Disconnected"
                elif state == False:
                    return False
                elif msg == "yes":
                    state = sendMessage("Thanks for telling me your name, "+name+", on to the next step!")

                    if state == "Disconnected":
                        return "Disconnected"

                    correctName = True
                    break
                elif msg == "no":
                    state = sendMessage("Ok lets try that one again, simply enter your name below!")

                    if state == "Disconnected":
                        return "Disconnected"

                    break
                else:
                    state = sendMessage("Sorry I don't understand that message. Try typing 'Yes' if the name you've provided is correct, or 'No' if the name you provided is not correct.")

                    if state == "Disconnected":
                        return "Disconnected"

            if correctName == True:
                break

    sleep(msgPause)
    state = sendMessage("Okay "+name+", how old are you? You can also type 'Back' to go back to the main options.")

    if state == "Disconnected":
        return "Disconnected"

    while True:
        state, msg = waitForMessage(120)
        lowerMsg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        elif lowerMsg == "back":
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"

            return True
        else:
            age = msg
            state = sendMessage("Are you "+age+" years old? Type 'Yes' or 'No'")

            if state == "Disconnected":
                return "Disconnected"

            correctAge = False

            while True:
                state, msg = waitForMessage(120)
                msg = msg.lower()

                if state == "Disconnected":
                    return "Disconnected"
                elif state == False:
                    return False
                elif msg == "yes":
                    state = sendMessage("Thanks for telling me your age "+name+"!")

                    if state == "Disconnected":
                        return "Disconnected"

                    correctAge = True
                    break
                elif msg == "no":
                    state = sendMessage("Ok lets try that one again, simply enter your age below!")

                    if state == "Disconnected":
                        return "Disconnected"

                    break
                else:
                    state = sendMessage("Sorry I don't understand that message. Try typing 'Yes' if the age you've provided is correct, or 'No' if the age you provided is not correct.")

                    if state == "Disconnected":
                        return "Disconnected"

            if correctAge == True:
                break

    sleep(msgPause)
    state = sendMessage("Where do you live, "+name+"? You can also type 'Back' to go back to the main options.")

    if state == "Disconnected":
        return "Disconnected"

    while True:
        state, msg = waitForMessage(120)
        lowerMsg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        elif lowerMsg == "back":
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"

            return True
        else:
            location = msg
            state = sendMessage("Do you live in "+location+"? Type 'Yes' or 'No'")

            if state == "Disconnected":
                return "Disconnected"

            correctCountry = False

            while True:
                state, msg = waitForMessage(120)
                msg = msg.lower()

                if state == "Disconnected":
                    return "Disconnected"
                elif state == False:
                    return False
                elif msg == "yes":
                    state = sendMessage("Thanks for telling me where you live "+name+"!")

                    if state == "Disconnected":
                        return "Disconnected"

                    correctCountry = True
                    break
                elif msg == "no":
                    state = sendMessage("Ok lets try that one again, simply enter the country you live in below!")

                    if state == "Disconnected":
                        return "Disconnected"

                    break
                else:
                    state = sendMessage("Sorry I don't understand that message. Try typing 'Yes' if the country you've provided is correct, or 'No' if the country you provided is not correct.")

                    if state == "Disconnected":
                        return "Disconnected"

            if correctCountry == True:
                break

    sleep(msgPause)
    state = sendMessage("Okay, please enter a kik or skype username below, and indicate which one it is. Alternatively you can type 'Back' to go back to the main options.")

    if state == "Disconnected":
        return "Disconnected"

    while True:
        state, msg = waitForMessage(180)
        lowerMsg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        elif lowerMsg == "back":
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"

            return True
        else:
            skypeUsr = msg
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"

            correctUsr = False

            while True:
                state, msg = waitForMessage(120)
                msg = msg.lower()

                if state == "Disconnected":
                    return "Disconnected"
                elif state == False:
                    return False
                elif msg == "yes":
                    state = sendMessage("Default")

                    if state == "Disconnected":
                        return "Disconnected"

                    correctUsr = True
                    break
                elif msg == "no":
                    state = sendMessage("Default")

                    if state == "Disconnected":
                        return "Disconnected"

                    break
                else:
                    state = sendMessage("Sorry I don't understand that message. Try typing 'Yes' if the username you've provided is correct, or 'No' if the username you provided is not correct.")

                    if state == "Disconnected":
                        return "Disconnected"

            if correctUsr == True:
                break

    sleep(msgPause)
    state = sendMessage("Okay "+name+", finally enter a paragraph about yourself. Please try to include as much detail as I gave you about my owner. Include anything you want him to know. You'll have up to 10 minutes to write this message so take your time. You'll also be given a one minute warning when you've got a minute left. Write about youself below, its easy! You can also type 'Back' to go back to the main options.")

    if state == "Disconnected":
        return "Disconnected"

    while True:
        state, msg = waitForMessage(600)
        lowerMsg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        elif lowerMsg == "back":
            state = sendMessage("Okay, you can now type 'About Him' to find out more about my owner, 'Feedback' to leave feedback about this chat bot, or 'Technical Information' to get technical information about this chat bot.")

            if state == "Disconnected":
                return "Disconnected"

            return True
        else:
            partnerDesc = msg
            state = sendMessage("Is what you've said above all you want to say "+name+"? Type 'Yes' or 'No'")

            if state == "Disconnected":
                return "Disconnected"

            correctDesc = False

            while True:
                state, msg = waitForMessage(120)
                msg = msg.lower()

                if state == "Disconnected":
                    return "Disconnected"
                elif state == False:
                    return False
                elif msg == "yes":
                    state = sendMessage("Default")

                    if state == "Disconnected":
                        return "Disconnected"

                    appendToSave("request")
                    correctDesc = True
                    break
                elif msg == "no":
                    state = sendMessage("Ok lets try that one again, simply write about yourself below!")

                    if state == "Disconnected":
                        return "Disconnected"

                    break
                else:
                    state = sendMessage("Sorry I don't understand that message. Try typing 'Yes' if the information you've provided about yourself is correct, or 'No' if it is not.")

                    if state == "Disconnected":
                        return "Disconnected"

            if correctDesc == True:
                break



def technicalInfoMsg():
    state = sendMessage("This bot is currently in version 1.5")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("My owner is always looking for ways to improve this bot, be it a bug report or suggestion, feel free to use the feedback feature to give suggestions or bug reports. He is also looking for someone to help him work on the bot so if you're interested then please leave details here.")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("This bot is wrriten in Python 2.7.9 using the Selenium API. It also uses Chromedriver, an add on for Selenium that allows it to interact with Google Chrome. If you search Selenium on Google you'll find all the relevant documentation.")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("It uses the webdriver object within Selenium to interact with webpages. Picking them apart element by element, using css class names and css selectors to find the element it is interested in. It also uses the click and send_keys functions to send messages and start new chats.")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Currently my owner is thinking of making a complex GUI that uses flow diagrams to make the bot easier to change on the fly, so messages don't have to be edited within the code. If you have experience in making GUI's with tkinter or another python GUI API then my owner will be interested in talking to you.")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("If you have skills in coding, particullarly in coding GUI's and are interested please feel free to leave contact details. Alternatively if you have more questions, also feel free to contact my owner. Please note you will not be allowed to see the source code of the bot unless you are planning on working on it with him and have proven skills in python.")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("He may however make a bot for you if you desire. Depending on what you want the bot to do there may be a cost to it, but if it is very basic and you plan on using it for good intentions he may make one for free, talk about this when you contact him if you are interested.")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Type 'Contact' to contact my owner with technical questions or to lend him a helping hand, or type 'Back' to go back for the feedback and about him options.")

    if state == "Disconnected":
        return "Disconnected"

    while True:
        state, msg = waitForMessage(120)
        msg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        elif msg == "contact":
            state = technicalContact()

            if state == "Disconnected":
                return "Disconnected"
            elif state == False:
                return False

            break
        elif msg == "back":
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"

            break
        else:
            state = sendMessage("Sorry I don't understand that message. Try typing 'Contact' to contact my owner with technical questions, or to lend a helping hand. Or type 'Back' to go back for the about him and feedback options.")

            if state == "Disconnected":
                return "Disconnected"



def technicalContact():
    global skypeUsr
    global partnerDesc

    state = sendMessage("Okay, to contact my owner you'll need to provide an email address or skype username, along with a paragraph explaining what questions you have if you're asking questions.")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("Alternatively if you're looking to lend a helping hand with the bot, you'll need to include your name, age, the country where you live and what coding knowledge you have and how you can be of help.")

    if state == "Disconnected":
        return "Disconnected"

    sleep(msgPause)
    state = sendMessage("To contiune type an email address or skype username. My owner will know which is which depending on if you include an @ sign or not. Alternatively type 'back' to go back to the main options.")

    if state == "Disconnected":
        return "Disconnected"

    while True:
        state, msg = waitForMessage(120)
        lowerMsg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        elif lowerMsg == "back":
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"

            return True
        else:
            skypeUsr = msg
            state = sendMessage("Is your email addres/skype username "+skypeUsr+"? Type 'Yes' or 'No'")

            if state == "Disconnected":
                return "Disconnected"

            correctSkypeUsr = False

            while True:
                state, msg = waitForMessage(120)
                msg = msg.lower()

                if state == "Disconnected":
                    return "Disconnected"

                elif state == False:
                    return False
                elif msg == "yes":
                    state = sendMessage("Okay thanks for giving me some contact details!")

                    if state == "Disconnected":
                        return "Disconnected"

                    correctSkypeUsr = True
                    break
                elif msg == "no":
                    state = sendMessage("Okay, lets try that again, type your email address or skype username below. Alternatively type 'Back' to go back to other options.")

                    if state == "Disconnected":
                        return "Disconnected"

                    break
                else:
                    state = sendMessage("Sorry I don't understand. Type 'Yes' if the email address or skype username you entered is correct, or 'No' if it is not correct.")

                    if state == "Disconnected":
                        return "Disconnected"

            if correctSkypeUsr == True:
                break

    state = sendMessage("Okay, lastly please enter a paragraph asking any questions you have, or telling my owner about you if you want to help code this chat bot. This should include your name, age, the country where you live, your knowledge of coding and how you can help with this bot. You'll have 10 minutes to enter as much as you want before I disconnect, I'll also give you a 1 minute warning so feel free to take your time. Alternatively type 'Back' to go back for more options.")

    if state == "Disconnected":
        return "Disconnected"

    while True:
        state, msg = waitForMessage(600)
        lowerMsg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        elif lowerMsg == "back":
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"

            return True
        else:
            partnerDesc = msg

            state = sendMessage("Is the information you've entered above all you want to say? Type 'Yes' or 'No'")

            if state == "Disconnected":
                return "Disconnected"

            correctPartnerDesc = False

            while True:
                state, msg = waitForMessage(120)
                msg = msg.lower()

                if state == "Disconnected":
                    return "Disconnected"
                elif state == False:
                    return False
                elif msg == "yes":
                    state = sendMessage("Okay, thanks! This information has been recorded and my owner will get back to you as soon as he can. In the mean time feel free to type 'About him' to find out more about my owner, 'Feedback' to leave feedback or feel free to disconnect and have a great day! :)")

                    if state == "Disconnected":
                        return "Disconnected"

                    correctPartnerDesc = True
                    appendToSave("technical")
                    break
                elif msg == "no":
                    state = sendMessage("Okay, let's try that again, enter the information you want my owner to know about you below!")

                    if state == "Disconnected":
                        return "Disconnected"

                    break
                else:
                    state = sendMessage("Sorry, I don't understand that message. Try typing 'Yes' if the information you've entered is all you want to say, or 'No' if it isn't.")

                    if state == "Disconnected":
                        return "Disconnected"

            if correctPartnerDesc == True:
                break


def getFeedback():
    global feedback

    state = sendMessage("Thanks for taking the time to leave feedback about this chat bot, be it a bug report or a suggestion. Please note that with this feedback the entire chat log will be submitted to aid your feedback if you send in a bug report. Just enter your feedback below, you'll be asked to confirm if it is correct afterwards and it will be submitted. You'll have up to 5 minutes to enter this feedback and be given a 1 minute warning, so take your time. Alternatively you can type 'Back' to go back to the main options.")

    if state == "Disconnected":
        return "Disconnected"

    while True:
        state, msg = waitForMessage(300)
        lowerMsg = msg.lower()

        if state == "Disconnected":
            return "Disconnected"
        elif state == False:
            return False
        elif lowerMsg == "back":
            state = sendMessage("Default")

            if state == "Disconnected":
                return "Disconnected"

            return True
        else:
            feedback = msg
            state = sendMessage("Is the feedback you've entered above all you want to say? Type 'Yes' or 'No'")

            if state == "Disconnected":
                return "Disconnected"

            correctFeedback = False

            while True:
                state, msg = waitForMessage(120)
                msg = msg.lower()

                if state == "Disconnected":
                    return "Disconnected"
                elif state == False:
                    return False
                elif msg == "yes":
                    state = sendMessage("Okay thanks for leaving your feedback, it has been submittied and my owner will take it into consideration. You can now type 'About Him' to find out more about my owner, type 'Technical Information' to find out about this chat bot or you can disconnect :)")

                    if state == "Disconnected":
                        return "Disconnected"

                    appendToSave("feedback")
                    correctFeedback = True
                    break
                elif msg == "no":
                    state = sendMessage("Okay, lets try that again. Simply enter your feedback below!")

                    if state == "Disconnected":
                        return "Disconnected"

                    break
                else:
                    state = sendMessage("Sorry I don't understand that message, try typing 'Yes' if the feedback you've written is all you want to say, or 'No' if it is not.")

                    if state == "Disconnected":
                        return "Disconnected"

            if correctFeedback == True:
                break



def appendToSave(type):
    global requestNum
    global technicalNum
    global feedbackNum

    if type == "request":
        saveFile = open("Request"+str(requestNum)+".txt", "w")
        requestNum += 1

        saveFile.write("Name: "+name+"\n")
        saveFile.write("Age: "+age+"\n")
        saveFile.write("Location: "+location+"\n")
        saveFile.write("Contact details: "+skypeUsr+"\n")
        saveFile.write("Bio: "+partnerDesc+"\n")
    elif type == "technical":
        saveFile = open("Technical"+str(technicalNum)+".txt", "w")
        technicalNum += 1

        saveFile.write("Contact details: "+skypeUsr+"\n")
        saveFile.write("Info: "+partnerDesc+"\n")
    else:
        saveFile = open("Feedback"+str(feedbackNum)+".txt", "w")
        feedbackNum += 1

        saveFile.write("Feedback:"+feedback+"\n"+"\n")
        saveFile.write("Chat Log: "+"\n")
        updateLog()
        saveFile.write(log)

    saveFile.close()



def mainSequence():
    startChatBot()
    waitForChat()
    newConvo = True

    while True:

        if newConvo == True:
            newConvo = False

            if openingMessage() == "Disconnected":
                newConvo = True
                newChat(True)
                waitForChat()
            else:
                state = mainUsrMenu()

                if state == "Disconnected":
                    newConvo = True
                    newChat(True)
                    waitForChat()
                elif state == False:
                    newConvo = True
                    newChat(False)
                    waitForChat()

        else:
            state = mainUsrMenu()

            if state == "Disconnected":
                newConvo = True
                newChat(True)
                waitForChat()
            elif state == False:
                newConvo = True
                newChat(False)
                waitForChat()


mainSequence()