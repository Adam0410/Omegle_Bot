This project is one that I consider to be completed, as such this is its final form and you will not see any commits or updates.

The purpose of this project is to create a bot that uses Selenium to interact with a one on one random chat site called Omegle: http://www.omegle.com/
The code has no comments as I wrote it back in a time where I did not understand the importance of comments, and since this is just here as an example of my
work I do not feel a need to add comments. I feel the code is written in a way that makes it clean(ish) to read and the variable and function names are named
well enough to be easy to keep track of and understand their purpose.

Before I explain the purpose of the program it is important to understand that the bot was written as a side project, soley for my own use and thus all things
that a user might want to change are hard coded in and I never developed a GUI or ability to change settings without modifying the source code since it was 
for my use only. 

It is also important to note the large majority of messages have been replaced by 'default' when otherwise they would be a string of text
with some message to be sent to the chat partner. Also most if statements that check the contents of the reply from the chat partner have also been changed to 
default, when in use it would have been something more meaningful. I did this since I did not want the personal contents of these messages revealed to all on
the internet (despite the fact that could have happened while in use) and also because several versions of the script exist all for different purposes and thus
the messages would be different.

Finally and most importantly, the bot does have bugs and won't work without a chromedriver.exe in a directory specified in the source code. If you wish to 
run the code you will need a python interpreter, or to compile it in python 2.7.10, and have a coppy of chromedriver.exe which can be found here: 
http://chromedriver.storage.googleapis.com/index.html?path=2.20/

The code also heavily relies on the selenium API for python, and it will be difficult to understand without knowing the API. Relevant documentation can be
found here: http://www.seleniumhq.org/docs/03_webdriver.jsp

The bot uses selenium and usually finds elements either by xpath or by css class names. The bot begins by going to the site, and entering interest tags on the
homepage. It enters the interest tags that are dictated by the topics list at the top of the code. It then proceeds to find a chat and start chatting. 
It has checks to make sure the chat partner has a similar interest tag before chatting. If the partner does not then it will automatically find a new chat.
It will then begin to display the opening messages, and then take input from the chat partner. What it does next is then dependant on the chat partner but it
contained several options. The main option was to find chat partners for some purpose (e.g. to play a video game with me) it would then ask the chat partner for
details if they wanted to contact me. It would then take these details and save that as a text file in the same folder as the script. I saved each individual
set of details/feedback as an individual file so negative, abusive or irrelevant comments could easily be deleted with two button presses.

Throughout the program there are also reduancies built in that detects if the chat partner has disconnected, and if so will find a new chat so it does not 
halt. It also has redudancies where it will automatically find a new chat if the chat partner takes too long to reply and therefore it can be assumed they 
are AFK (Away from keyboard).

There are also lots of other features built in so go and take a look!